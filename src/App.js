import React from "react";

import Routing from "../src/common/routes/routing";

const App = () => {
  return <Routing />;
};

export default App;
