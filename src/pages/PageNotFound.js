import React from "react";
import { useNavigate } from "react-router-dom";

const PageNotFound = () => {
  let navigate = useNavigate();

  return (
    <div className="flex items-center justify-center flex-col min-h-screen bg-slate-200">
      <span className="text-6xl text-green-500">This Page Doesn't Exist</span>
      <button
        className="bg-green-500 text-white px-4 py-2 mt-10 rounded text-2xl hover:bg-green-600 transition duration-300"
        onClick={() => {
          navigate("/");
        }}
      >
        Go Back Home
      </button>
    </div>
  );
};

export default PageNotFound;
