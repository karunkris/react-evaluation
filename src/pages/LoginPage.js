import React, { useRef, useState } from "react";
import { useNavigate } from "react-router-dom";
import Alerts from "../components/alerts";
import LoginCard from "../components/loginCard";
import { setItem } from "../common/services/localStorageService";

const LoginPage = () => {
  let navigate = useNavigate();

  // for setting error messages
  const [title, setTitle] = useState("");
  const [message, setMessage] = useState("");

  const alertRef = useRef();

  const loginUser = (email, password) => {
    // getting the data in JSON format
    const userData = JSON.parse(localStorage.getItem("users")) || {};

    // verifying the login
    if (email in userData) {
      if (userData[email].password === password) {
        const data = {
          email: userData[email].email,
          name: userData[email].name,
          country: userData[email].country,
        };

        setItem("activeUser", data);
        navigate("/home");
      } else {
        setMessage("Incorrect Password");
        setTitle("Login Failed");
        alertRef.current.show();
      }
    } else {
      setMessage("Email address doesn't exist");
      setTitle("Login Failed");
      alertRef.current.show();
    }
  };

  return (
    <>
      <Alerts title={title} message={message} success={false} ref={alertRef} />
      <LoginCard onLogin={loginUser} />;
    </>
  );
};

export default LoginPage;
