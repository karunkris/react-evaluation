import React, { useState } from "react";
import Paginate from "../components/homepage/paginate";
import CountryCard from "../components/homepage/countryCard";
import Sidebar from "../components/homepage/sidebar";
import Backdrop from "../common/backdrop";
import ViewDetails from "../components/homepage/viewDetails";
import { useCountry } from "../common/countryContext";

const HomePage = () => {
  const { countryData } = useCountry();

  const [currentPage, setCurrentPage] = useState(0);

  // Getting active user
  const data = localStorage.getItem("activeUser");
  const activeUser = JSON.parse(data);

  // FILTERING COUNTRY DATA
  const [filteredCountries, setFilteredCountries] = useState(countryData);
  const [selectedContinent, setSelectedContinent] = useState("All Countries");

  const filterCountries = (continent) => {
    if (continent === "All Countries") {
      setFilteredCountries(countryData);
      setSelectedContinent(continent);
      setCurrentPage(0);

      setIsFavouriteOpen(false);
    } else {
      const data = countryData.filter(
        (country) => country.continent === continent
      );
      setFilteredCountries(data);
      setSelectedContinent(continent);
      setCurrentPage(0);

      setIsFavouriteOpen(false);
    }
  };

  // PAGINATION LOGIC
  const countryPerPage = 6;

  const firstIndex = currentPage * countryPerPage;
  const lastIndex = firstIndex + countryPerPage;
  const selectedCountries = filteredCountries.slice(firstIndex, lastIndex);

  const pageCount = Math.ceil(filteredCountries.length / countryPerPage);

  const changePage = ({ selected }) => setCurrentPage(selected);

  // FAVOURITES LOGIC
  const [favourites, setFavourites] = useState([]);
  const [isFavouriteOpen, setIsFavouriteOpen] = useState(false);
  const [favouriteCountryNames, setFavouriteCountryNames] = useState([]);

  const [favouritePageCount, setFavouritePageCount] = useState(1);

  const addFavourite = (country) => {
    const favouriteCountries = [...favourites, country];
    const countryNames = [...favouriteCountryNames, country.name];
    setFavouritePageCount(Math.ceil(favourites.length / countryPerPage));
    setFavourites(favouriteCountries);
    setFavouriteCountryNames(countryNames);
  };

  const removeFavourites = (country) => {
    const newFavouriteList = favourites.filter(
      (favourite) => favourite.id !== country.id
    );
    const newCountryNames = favouriteCountryNames.filter(
      (favourite) => favourite !== country.name
    );
    setFavourites(newFavouriteList);
    setFavouriteCountryNames(newCountryNames);
  };

  const toggleFavouritePage = () => {
    setIsFavouriteOpen(true);
    setSelectedContinent("");
  };

  //PROFILE PAGE
  const [profileCard, setProfileCard] = useState(false);

  const viewProfile = () => {
    setProfileCard(true);
  };

  const closeDetails = () => {
    setProfileCard(false);
  };

  return (
    <div className="bg-slate-100 relative md:flex min-h-screen">
      <Sidebar
        filterCountries={filterCountries}
        favouritePage={toggleFavouritePage}
        viewProfile={viewProfile}
        selectedContinent={selectedContinent}
      />

      <div className="flex-1">
        {profileCard && <Backdrop close={closeDetails} />}
        {profileCard && (
          <ViewDetails
            name={activeUser.name}
            email={activeUser.email}
            country={activeUser.country}
          />
        )}

        <div className="grid lg:grid-cols-3 gap-5 mb-12">
          {isFavouriteOpen ? (
            <>
              <CountryCard
                removeFavourites={removeFavourites}
                countries={favourites.slice(firstIndex, lastIndex)}
                favouriteButton={false}
                favouriteCountries={favouriteCountryNames}
              />
              <Paginate
                pageCount={favouritePageCount}
                changePage={changePage}
                countryCount={favourites.length}
                currentPage={currentPage}
              />
            </>
          ) : (
            <>
              <CountryCard
                favourites={addFavourite}
                countries={selectedCountries}
                favouriteButton={true}
                favouriteCountries={favouriteCountryNames}
              />
              <Paginate
                pageCount={pageCount}
                changePage={changePage}
                countryCount={selectedCountries.length}
                currentPage={currentPage}
              />
            </>
          )}
        </div>
      </div>
    </div>
  );
};

export default HomePage;
