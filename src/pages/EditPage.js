import React, { useRef, useState } from "react";
import EditCard from "../components/editCard";
import Alerts from "../components/alerts";
import { useNavigate } from "react-router-dom";
import Backdrop from "../common/backdrop";
import ConfirmationPopup from "../components/homepage/confirmationPopup";
import {
  getItem,
  setItem,
  removeItem,
} from "../common/services/localStorageService";

const EditPage = () => {
  // getting active user details
  const activeUser = getItem("activeUser");

  // getting all users details
  const users = getItem("users");

  // alert message
  const alertRef = useRef(null);
  const [title, setTitle] = useState("");
  const [message, setMessage] = useState("");

  let navigate = useNavigate();

  // edit profile logic
  const editProfile = (data) => {
    if (data.newPassword === data.confirmPassword) {
      activeUser.name = data.newName;

      const userData = {
        email: activeUser.email,
        password: data.newPassword,
        name: activeUser.name,
        country: activeUser.country,
      };

      users[activeUser.email] = userData;
      setItem("activeUser", activeUser);
      setItem("users", users);
      navigate("/home");
    } else {
      setTitle("Updation Failed");
      setMessage("Passwords do not match");
      alertRef.current.show();
    }
  };

  // delete profile logic
  const [confirmPopup, setConfirmPopup] = useState(false);

  // showing and hiding the delete confirmation popup
  const deleteConfirmation = () => setConfirmPopup(true);
  const closePopup = () => setConfirmPopup(false);

  const deleteProfile = () => {
    delete users[activeUser.email];
    setItem("users", users);
    removeItem("activeUser");
    navigate("/");
  };

  return (
    <>
      <EditCard onEdit={editProfile} deleteConfirmation={deleteConfirmation} />;
      <Alerts title={title} message={message} success={false} ref={alertRef} />
      {confirmPopup && (
        <ConfirmationPopup closePopup={closePopup} delete={deleteProfile} />
      )}
      {confirmPopup && <Backdrop close={closePopup} />}
    </>
  );
};

export default EditPage;
