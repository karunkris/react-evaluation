import React, { useRef, useState } from "react";
import { useNavigate } from "react-router-dom";
import Alerts from "../components/alerts";
import SignupCard from "../components/signupCard";
import { setItem } from "../common/services/localStorageService";

const SignupPage = () => {
  let navigate = useNavigate();

  const alertRef = useRef();

  // setting up error message
  const [title, setTitle] = useState("");
  const [message, setMessage] = useState("");

  var users = JSON.parse(localStorage.getItem("users")) || {};

  // creates an object with email ID as the key
  const signupUser = (email, confirmPass, data) => {
    if (email in users) {
      setTitle("Registration failed");
      setMessage("Email already exists");
      alertRef.current.show();
    } else {
      if (data.password === confirmPass) {
        users[email] = data;
        Object.assign({}, users, users[email]);
        setItem("users", users);

        const activeUser = {
          email: data.email,
          name: data.name,
          country: data.country,
        };

        setItem("activeUser", activeUser);
        navigate("/home");
      } else {
        setTitle("Registration failed");
        setMessage("Passwords do not match");
        alertRef.current.show();
      }
    }
  };

  return (
    <>
      <Alerts title={title} message={message} success={false} ref={alertRef} />
      <SignupCard onSignup={signupUser} />
    </>
  );
};

export default SignupPage;
