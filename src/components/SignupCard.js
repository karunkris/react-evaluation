import React, { useRef } from "react";
import { Link } from "react-router-dom";
import Countries from "../common/countries";

const SignupCard = (props) => {
  // refs for storing input
  const emailRef = useRef();
  const passwordRef = useRef();
  const confirmPassword = useRef();
  const nameRef = useRef();
  const countryRef = useRef();

  const signup = (event) => {
    event.preventDefault();

    // contains user data
    const userData = {
      email: emailRef.current.value,
      password: passwordRef.current.value,
      name: nameRef.current.value,
      country: countryRef.current.value,
    };
    props.onSignup(
      emailRef.current.value,
      confirmPassword.current.value,
      userData
    );
  };

  return (
    <div className="min-h-screen bg-slate-200 flex flex-col justify-center">
      <span className="border text-4xl text-green-500 px-8 py-8 bg-white w-2/3 max-w-md mx-auto rounded">
        Signup for an account
      </span>

      <div className="border relative px-8 pt-7 pb-8 bg-white shadow-xl w-2/3 max-w-md mx-auto rounded">
        <form onSubmit={signup}>
          <label className="block mb-1">Email Address</label>
          <input
            type="Email"
            className="border w-full h-10 px-3 mb-5 rounded outline-none focus:border-green-500"
            placeholder="Email"
            ref={emailRef}
            required
          />

          <label className="block mb-1">Display Name</label>
          <input
            type="text"
            className="border w-full h-10 px-3 mb-5 rounded outline-none focus:border-green-500"
            placeholder="Display Name"
            ref={nameRef}
            required
          />

          <label className="block mb-1">Password</label>
          <input
            type="password"
            className="border w-full h-10 px-3 mb-5 rounded outline-none focus:border-green-500"
            placeholder="Password"
            maxLength={"15"}
            ref={passwordRef}
            required
          />

          <label className="block mb-1">Confirm Password</label>
          <input
            type="password"
            className="border w-full h-10 px-3 mb-5 rounded outline-none focus:border-green-500"
            placeholder="Password"
            maxLength={"15"}
            ref={confirmPassword}
            required
          />

          <label className="block mb-1">Country</label>
          <select
            className="border w-full h-10 px-3 mb-5 rounded outline-none focus:border-green-500"
            placeholder="Country"
            ref={countryRef}
            required
          >
            {<Countries />}
          </select>

          <button className="mt-2 bg-green-500 hover:bg-green-800 shadow-xl text-white text-lg font-semibold px-14 py-3 transition duration-300 rounded w-full">
            SIGNUP
          </button>

          <div className="flex mt-6 justify-center">
            <Link to={"/"} className="text-md text-green-500 hover:underline">
              Already Have an Account? Login Here
            </Link>
          </div>
        </form>
      </div>
    </div>
  );
};

export default SignupCard;
