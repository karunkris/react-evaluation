import React from "react";
import ReactPaginate from "react-paginate";

const Paginate = ({ pageCount, changePage, countryCount, currentPage }) => {
  const count = countryCount > 0 ? true : false;
  return (
    <>
      {count ? (
        <div className="absolute inset-x-0 bottom-4">
          <ReactPaginate
            previousLabel="Previous"
            nextLabel="Next"
            pageCount={pageCount}
            onPageChange={changePage}
            forcePage={currentPage}
            containerClassName={"flex justify-center gap-3 pageButtons mb-4"}
            previousLinkClassName={
              "uppercase font-medium hover:text-green-700 transition duration-200"
            }
            nextLinkClassName={
              "uppercase font-medium hover:text-green-700 transition duration-200"
            }
            activeClassName={"bg-green-600 text-white"}
            disabledClassName={"text-gray-400"}
          />
        </div>
      ) : (
        <div className="absolute top-20 left-80">
          <span className="text-2xl text-gray-600">
            No Countries Favourited!
          </span>
        </div>
      )}
    </>
  );
};

export default React.memo(Paginate);
