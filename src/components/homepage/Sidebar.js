import React from "react";
import { useNavigate } from "react-router-dom";
import { useCountry } from "../../common/countryContext";
import { removeItem } from "../../common/services/localStorageService";

const Sidebar = ({
  filterCountries,
  favouritePage,
  viewProfile,
  selectedContinent,
}) => {
  let navigate = useNavigate();
  const { countryData } = useCountry();

  // getting continent list from the data
  const continents = [
    "All Countries",
    ...new Set(countryData.map((country) => country.continent)),
  ];

  // displaying each continent in the sidebar
  const displayContinents = continents.map((continent, index) => {
    if (selectedContinent === continent) {
      return (
        <div key={index}>
          <button
            className="block py-2.5 px-4 bg-white text-green-700 text-lg font-medium transition duration-200 hover:bg-gray-200"
            onClick={() => filterCountries(continent)}
          >
            {continent}
          </button>
        </div>
      );
    } else {
      return (
        <div key={index}>
          <button
            className="block py-2.5 px-4 text-white text-lg font-medium transition duration-200 hover:bg-white hover:text-green-700"
            onClick={() => filterCountries(continent)}
          >
            {continent}
          </button>
        </div>
      );
    }
  });

  return (
    <>
      <div className="flex flex-col bg-green-600 w-1/6 space-y-6 py-7 px-2 absolute inset-y-0 left-0 transform -translate-x-full md:relative md:translate-x-0 transition duration-200 ease-in-out">
        <nav>{displayContinents}</nav>

        <div className="flex flex-col gap-3">
          <button
            className="mb-20 mt-4 bg-white py-2 text-lg font-medium text-green-600 hover:bg-slate-200 transition duration-200"
            onClick={favouritePage}
          >
            Favourites
          </button>

          <button
            className="border-2 border-white text-white py-2 rounded text-md font-medium hover:bg-white hover:text-green-600 transition duration-200"
            onClick={viewProfile}
          >
            VIEW PROFILE
          </button>

          <button
            className="bg-white text-green-600 py-2 rounded text-md font-medium transition duration-200 hover:bg-slate-200"
            onClick={() => {
              removeItem("activeUser");
              navigate("/");
            }}
          >
            SIGN OUT
          </button>
        </div>
      </div>
    </>
  );
};

export default React.memo(Sidebar);
