import React from "react";
import { useNavigate } from "react-router-dom";

const ViewDetails = ({ email, name, country }) => {
  let navigate = useNavigate();
  return (
    <div className="min-w-screen h-screen flex justify-center items-center inset-0 absolute">
      <div className="shadow-lg bg-white text-center z-50 w-1/3">
        <div className="py-6 px-6 text-xl bg-green-600 text-white">
          Profile Details
        </div>
        <div className="p-4">
          <h5 className="text-gray-900 text-xl font-bold mb-4">{name}</h5>
          <p className="text-gray-700 text-base mb-4">
            <strong>Email ID: </strong> {email}
          </p>
          <p className="text-gray-700 text-base mb-4">
            <strong>Country: </strong> {country}
          </p>
          <button
            type="button"
            className=" inline-block px-6 py-2.5 mt-2 bg-green-600 text-white font-medium text-md rounded hover:bg-green-700 transition duration-150 ease-in-out"
            onClick={() => navigate("/edit")}
          >
            UPDATE PROFILE
          </button>
        </div>
      </div>
    </div>
  );
};

export default ViewDetails;
