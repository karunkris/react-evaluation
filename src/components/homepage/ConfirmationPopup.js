import React from "react";

const ConfirmationPopup = (props) => {
  return (
    <div className="min-w-screen h-screen animated fadeIn faster fixed flex justify-center items-center inset-0 z-50">
      <div className="max-w-lg p-5 rounded shadow-lg bg-white">
        <div>
          <div className="text-center p-5 flex-auto justify-center">
            <h2 className="text-xl font-bold py-4 text-red-500">
              Are you sure?
            </h2>
            <p className="text-md px-8">
              Do you really want to delete your account? This process cannot be
              undone
            </p>
          </div>
          <div className="p-3  mt-2 text-center space-x-4 md:block">
            <button
              className="mb-2 md:mb-0 bg-white px-5 py-2 text-md font-medium border text-gray-600 rounded hover:bg-gray-100"
              onClick={props.closePopup}
            >
              CANCEL
            </button>
            <button
              className="mb-2 md:mb-0 bg-red-500 border border-red-500 px-5 py-2 text-md font-medium text-white rounded hover:bg-red-600"
              onClick={props.delete}
            >
              DELETE
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ConfirmationPopup;
