import React from "react";

const CountryCard = ({
  countries,
  favourites,
  removeFavourites,
  favouriteButton,
  favouriteCountries,
}) => {
  const displayCountries = countries.map((country) => {
    if (favouriteCountries.includes(country.name)) {
      return (
        <div className="flex justify-center mt-8 mx-6" key={country.id}>
          <div className="block p-6 rounded-lg shadow-lg bg-white max-w-sm sm:mb-6">
            <h5 className="text-green-700 text-3xl leading-tight font-medium mb-2">
              {country.name}
            </h5>

            <p className="text-gray-700 text-md mb-4">
              {country.name} ({country.abbr3}) is a country in{" "}
              {country.continent}. The official currency of {country.name} is{" "}
              {country.currencyName} ({country.currencyCode})
            </p>

            {favouriteButton ? (
              <button
                type="button"
                className="inline-block px-6 py-2.5 bg-green-800 text-white text-md rounded shadow-m"
                disabled={true}
              >
                Favourited
              </button>
            ) : (
              <button
                type="button"
                className="inline-block px-6 py-2.5 bg-green-600 text-white text-md rounded shadow-md hover:bg-green-700 transition duration-150 ease-in-out"
                onClick={() => removeFavourites(country)}
              >
                Remove Favourite
              </button>
            )}
          </div>
        </div>
      );
    } else {
      return (
        <div className="flex justify-center mt-8 mx-6" key={country.id}>
          <div className="block p-6 rounded-lg shadow-lg bg-white max-w-sm sm:mb-6">
            <h5 className="text-green-700 text-3xl leading-tight font-medium mb-2">
              {country.name}
            </h5>

            <p className="text-gray-700 text-md mb-4">
              {country.name} ({country.abbr3}) is a country in{" "}
              {country.continent}. The official currency of {country.name} is{" "}
              {country.currencyName} ({country.currencyCode})
            </p>

            {favouriteButton ? (
              <button
                type="button"
                className="inline-block px-6 py-2.5 bg-green-600 text-white text-md rounded shadow-md hover:bg-green-700 transition duration-150 ease-in-out"
                onClick={() => favourites(country)}
              >
                Add to Favourite
              </button>
            ) : (
              <button
                type="button"
                className="inline-block px-6 py-2.5 bg-green-600 text-white text-md rounded shadow-md hover:bg-green-700 transition duration-150 ease-in-out"
                onClick={() => removeFavourites(country)}
              >
                Remove Favourite
              </button>
            )}
          </div>
        </div>
      );
    }
  });

  return <>{displayCountries}</>;
};

export default CountryCard;
