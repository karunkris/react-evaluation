import React, { useRef } from "react";
import { Link } from "react-router-dom";

const EditCard = (props) => {
  const newNameRef = useRef();
  const newPasswordRef = useRef();
  const confirmPasswordRef = useRef();

  const updateProfile = (e) => {
    e.preventDefault();
    const newData = {
      newName: newNameRef.current.value,
      newPassword: newPasswordRef.current.value,
      confirmPassword: confirmPasswordRef.current.value,
    };
    props.onEdit(newData);
  };

  return (
    <div className="min-h-screen bg-slate-200 flex flex-col justify-center">
      <span className="border text-4xl text-green-500 px-8 py-8 bg-white w-2/3 max-w-md mx-auto rounded">
        Edit Your Profile
      </span>

      <div className="border relative px-8 pt-7 pb-8 bg-white shadow-xl w-2/3 max-w-md mx-auto rounded">
        <form>
          <label className="block mb-1">New Display Name</label>
          <input
            type="text"
            className="border w-full h-10 px-3 mb-5 rounded outline-none focus:border-green-500"
            placeholder="Display Name"
            ref={newNameRef}
            required
          />

          <label className="block mb-1">New Password</label>
          <input
            type="password"
            className="border w-full h-10 px-3 mb-5 rounded outline-none focus:border-green-500"
            placeholder="Password"
            maxLength={"15"}
            ref={newPasswordRef}
            required
          />

          <label className="block mb-1">Confirm Password</label>
          <input
            type="password"
            className="border w-full h-10 px-3 mb-5 rounded outline-none focus:border-green-500"
            placeholder="Confirm Password"
            maxLength={"15"}
            ref={confirmPasswordRef}
            required
          />

          <div className="flex gap-4">
            <button
              className="mt-2 bg-green-500 hover:bg-green-800 shadow-xl text-white text-md font-semibold py-3 transition duration-300 rounded flex-1"
              onClick={updateProfile}
            >
              UPDATE
            </button>

            <button
              className="mt-2 bg-red-500 hover:bg-red-800 shadow-xl text-white text-md font-semibold py-3 transition duration-300 rounded flex-1"
              onClick={props.deleteConfirmation}
            >
              DELETE
            </button>
          </div>

          <div className="flex mt-6 justify-center">
            <Link
              to={"/home"}
              className="text-md text-green-500 hover:underline"
            >
              Go Back Home
            </Link>
          </div>
        </form>
      </div>
    </div>
  );
};

export default EditCard;
