import React, { forwardRef, useImperativeHandle, useState } from "react";

const Alerts = forwardRef((props, ref) => {
  const [showAlert, setShowAlert] = useState(false);

  useImperativeHandle(ref, () => ({
    show() {
      setShowAlert(true);
      setTimeout(() => {
        setShowAlert(false);
      }, 2500);
    },
  }));

  return props.success ? (
    <div
      className={`bg-green-100 border-l-4 border-green-500 text-green-700 p-4 absolute top-10 right-10 w-1/5 ${
        showAlert ? "visible" : "invisible"
      }`}
    >
      <p className="font-bold">{props.title}</p>
      <p>{props.message}</p>
    </div>
  ) : (
    <div
      className={`bg-red-100 border-l-4 border-red-500 text-red-700 p-4 absolute top-10 right-10 w-1/5 ${
        showAlert ? "visible" : "invisible"
      }`}
    >
      <p className="font-bold">{props.title}</p>
      <p>{props.message}</p>
    </div>
  );
});

export default Alerts;
