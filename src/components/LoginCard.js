import React, { useRef } from "react";
import { Link } from "react-router-dom";

const LoginCard = (props) => {
  // refs for storing input
  const emailRef = useRef();
  const passwordRef = useRef();

  const login = (event) => {
    event.preventDefault();
    props.onLogin(emailRef.current.value, passwordRef.current.value);
  };

  return (
    <div className="min-h-screen bg-slate-200 flex flex-col justify-center">
      <span className="border text-4xl text-green-500 px-8 py-8 bg-white w-2/3 max-w-md mx-auto rounded">
        Login to your account
      </span>

      <div className="border relative px-8 pt-7 pb-8 bg-white shadow-xl w-2/3 max-w-md mx-auto rounded">
        <form onSubmit={login}>
          <label className="block mb-1">Email Address</label>
          <input
            type="Email"
            className="border w-full h-10 px-3 mb-5 rounded outline-none focus:border-green-500"
            placeholder="Email"
            ref={emailRef}
            required
          />

          <label className="block mb-1">Password</label>
          <input
            type="password"
            className="border w-full h-10 px-3 mb-5 rounded outline-none focus:border-green-500"
            placeholder="Password"
            maxLength={15}
            ref={passwordRef}
            required
          />

          <button className="mt-2 bg-green-500 hover:bg-green-800 shadow-xl text-white text-lg font-semibold px-14 py-3 transition duration-300 rounded w-full">
            LOGIN
          </button>

          <div className="flex mt-6 justify-center">
            <Link
              to={"/signup"}
              className="text-md text-green-500 hover:underline"
            >
              Don't have an Account? Signup Here
            </Link>
          </div>
        </form>
      </div>
    </div>
  );
};

export default LoginCard;
