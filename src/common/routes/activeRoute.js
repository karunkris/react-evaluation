import React from "react";
import { Navigate, Outlet } from "react-router-dom";
import { CountryContext } from "../countryContext";

const ActiveRoute = () => {
  const activeUser = localStorage.getItem("activeUser") ? true : false;
  return activeUser ? (
    <CountryContext>
      <Outlet />
    </CountryContext>
  ) : (
    <Navigate to={"/"} />
  );
};

export default ActiveRoute;
