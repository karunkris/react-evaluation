import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";

import LoginPage from "../../pages/loginPage";
import SignupPage from "../../pages/signupPage";
import HomePage from "../../pages/homePage";
import EditPage from "../../pages/editPage";
import PageNotFound from "../../pages/pageNotFound";
import ActiveRoute from "../routes/activeRoute";
import InactiveRoute from "../routes/inactiveRoute";

const Routing = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route element={<ActiveRoute />}>
          <Route path="/home" element={<HomePage />} />
          <Route path="/edit" element={<EditPage />} />
        </Route>
        <Route element={<InactiveRoute />}>
          <Route path="/" element={<LoginPage />} />
          <Route path="/signup" element={<SignupPage />} />
        </Route>

        <Route path="*" element={<PageNotFound />} />
      </Routes>
    </BrowserRouter>
  );
};

export default Routing;
