import React from "react";
import { Navigate, Outlet } from "react-router-dom";

const InactiveRoute = () => {
  const activeUser = localStorage.getItem("activeUser") ? true : false;

  return activeUser ? <Navigate to={"/home"} /> : <Outlet />;
};

export default InactiveRoute;
