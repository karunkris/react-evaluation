import { createContext, useContext, useEffect, useState } from "react";
import { fetchData } from "./services/apiFetchService";

const country = createContext();

export const useCountry = () => {
  return useContext(country);
};

export const CountryContext = ({ children }) => {
  const [countryData, setCountryData] = useState();

  // loading ensures that page mounts only after getting api response
  const [loading, setLoading] = useState(true);

  // fetching all the country data
  useEffect(() => {
    const fetchCountries = async () => {
      try {
        const res = await fetchData();
        setCountryData(res.data.countries);
        setLoading(false);
      } catch (error) {
        console.log(error);
      }
    };

    fetchCountries();
  }, []);

  const value = { countryData };

  return (
    <country.Provider value={value}>{!loading && children}</country.Provider>
  );
};
