import axios from "axios";

const URL =
  "https://geoenrich.arcgis.com/arcgis/rest/services/World/geoenrichmentserver/Geoenrichment/countries?f=pjson";

export const fetchData = () => {
  return axios.get(URL);
};
