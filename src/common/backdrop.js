import React from "react";

const Backdrop = ({ close }) => {
  return (
    <div
      className="fixed top-0 left-0 z-10 h-full w-full bg-black opacity-90"
      onClick={close}
    />
  );
};

export default Backdrop;
